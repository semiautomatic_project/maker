package ldh.maker.component;

import javafx.scene.control.TreeItem;
import ldh.database.Column;
import ldh.database.Table;
import ldh.maker.code.CreateCode;
import ldh.maker.database.TableInfo;
import ldh.maker.freemaker.ControllerMaker;
import ldh.maker.freemaker.KeyMaker;
import ldh.maker.freemaker.SimpleJavaMaker;
import ldh.maker.util.CopyDirUtil;
import ldh.maker.util.DbInfoFactory;
import ldh.maker.util.FileUtil;
import ldh.maker.util.FreeMakerUtil;
import ldh.maker.vo.SettingData;
import ldh.maker.vo.TreeNode;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;

/**
 * Created by ldh on 2017/4/6.
 */
public class NodejsWebCreateCode extends CreateCode {

    private volatile boolean isCreateMainPage = false;

    public NodejsWebCreateCode(SettingData data, TreeItem<TreeNode> treeItem, String dbName, Table table) {
        super(data, treeItem, dbName, table);
    }

    @Override
    public void create() {
        if (data == null) return;
        if (!table.isCreate()) return;

        if (table.getPrimaryKey() == null || table.getPrimaryKey().isComposite()) {
            System.out.println("WARN!!!! " + table.getName() + " 是复合主键，软件暂时不支持复合主键");
            return;
        }

        if (isCreate) {
            createOnce();
            isCreate = false;
        }

        createOther();
    }

    @Override
    protected void createOther(){
        if (table.isMiddle()) return;
        String pagePath = createPath("src", "pages", FreeMakerUtil.firstLower(table.getJavaName()));
        String beanName = FreeMakerUtil.firstUpper(table.getJavaName());

        new SimpleJavaMaker().data("table", table)
                .fileName(beanName + "List.vue")
                .outPath(pagePath)
                .ftl("/web/List.ftl")
                .make();

        new SimpleJavaMaker().data("table", table)
                .fileName(beanName + "View.vue")
                .outPath(pagePath)
                .ftl("/web/View.ftl")
                .make();
    }

    @Override
    protected void createOnce() {
        makerFrame();

        List<String> dirs = new ArrayList<>(Arrays.asList("code", root, this.getProjectName(), "src", "components"));
        List<String> assetDirs = new ArrayList<>(Arrays.asList("code", root, this.getProjectName(), "src", "assets"));
        List<String> rootDirs = new ArrayList<>(Arrays.asList("code", root, this.getProjectName(), "src"));
        try {
            copyFile("ftl/web/Main.vue", dirs);
            copyFile("ftl/web/Pagination.vue", dirs);
            copyFile("ftl/web/TableView.vue", dirs);
            copyFile("ftl/web/App.vue", rootDirs);

            copyResources("common/web", assetDirs, "common");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void makerFrame() {
        TableInfo tableInfo = DbInfoFactory.getInstance().get(treeItem.getValue().getParent().getId() + "_" + dbName);
        String componentPath = createPath("src", "components");
        String routerPath = createPath("src", "router");
        new SimpleJavaMaker().data("tableInfo", tableInfo)
                .fileName("Left.vue")
                .outPath(componentPath)
                .ftl("/web/Left.ftl")
                .make();

        new SimpleJavaMaker().data("tableInfo", tableInfo)
                .fileName("Header.vue")
                .outPath(componentPath)
                .ftl("/web/Header.ftl")
                .make();

        new SimpleJavaMaker().data("tableInfo", tableInfo)
                .fileName("index.js")
                .outPath(routerPath)
                .ftl("/web/index.ftl")
                .make();
    }

    public String getProjectName() {
        return "nodejs-web";
    }

    private void copyResources(String srcDir, List<String> dirs) throws IOException {
        String root = FileUtil.getSourceRoot();
        Enumeration<URL> urls = Thread.currentThread().getContextClassLoader().getResources(srcDir);
        String srcFile = urls.nextElement().getFile();
        CopyDirUtil.copyResourceDir(srcFile, root, dirs);
    }
}
