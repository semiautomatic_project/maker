package ldh.maker.db;

import ldh.maker.util.UiUtil;
import ldh.maker.vo.SettingData;
import ldh.maker.vo.TableViewData;
import ldh.maker.vo.TreeNode;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ldh123 on 2018/5/5.
 */
public class TableViewDb {

    public static List<TableViewData> loadData(TreeNode treeNode, String dbName) throws SQLException {
        List<TableViewData> data = new ArrayList<>();
        Connection connection = UiUtil.H2CONN;
        QueryRunner queryRunner = new QueryRunner();
        String sql = "select id, table_name as tableName, db_name as dbName, alias, columns, tree_node_id as treeNodeId" +
                " from table_view where tree_node_id = ? and db_name = ?";
        ResultSetHandler<List<TableViewData>> h = new BeanListHandler<>(TableViewData.class);
        data = queryRunner.query(connection, sql, h, treeNode.getId(), dbName);
        return data;
    }

    public static void save(List<TableViewData> data) throws SQLException {
        for (TableViewData tvd : data) {
            save(tvd);
        }
    }

    public static void save(TableViewData data) throws SQLException {
        Connection connection = UiUtil.H2CONN;
        QueryRunner queryRunner = new QueryRunner();
        String sql = "insert into table_view(table_name, db_name, alias, columns, tree_node_id) values(?, ?, ?, ?, ?)";
        String updateSql = "update table_view set alias = ?, columns = ? where id = ?";

        if (data.getId() == null) {
            int i = 0;
            Object[] param = new Object[5];
            param[i++] = data.getTableName();
            param[i++] = data.getDbName();
            param[i++] = data.getAlias();
            param[i++] = data.getColumns();
            param[i++] = data.getTreeNodeId();
            Integer id = queryRunner.insert(connection, sql, new ScalarHandler<Integer>(1), param);
            data.setId(id);
        } else {
            queryRunner.update(connection, updateSql, data.getAlias(), data.getColumns(), data.getId());
        }
    }

    public static void delete(TreeNode treeNode) throws SQLException {
        Connection connection = UiUtil.H2CONN;
        QueryRunner queryRunner = new QueryRunner();
        String sql = "delete from table_view where tree_node_id = ?";
        queryRunner.update(connection, sql, treeNode.getId());
    }
}
