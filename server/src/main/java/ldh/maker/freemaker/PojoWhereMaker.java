package ldh.maker.freemaker;

import ldh.common.Orderable;
import ldh.database.Table;
import ldh.maker.database.TableInfo;
import ldh.maker.util.FreeMakerUtil;

import java.util.Date;


public class PojoWhereMaker extends BeanMaker<PojoWhereMaker> {

	public PojoWhereMaker() {
		this.implement(Orderable.class);
	}
	
	public PojoWhereMaker make() {
		super.data();
		out("beanWhere.ftl", data);
		
		return this;
	}

	public PojoWhereMaker serializable(boolean serializable) {
		this.serializable = serializable;
		return this;
	}
	
	public PojoWhereMaker imports(String imports) {
		this.imports.add(imports);
		return this;
	}
	
	public PojoWhereMaker extend(String extend) {
		this.extendsClassName = extend;
		return this;
	}
	
	public PojoWhereMaker extend(BeanMaker<?> maker) {
		this.extendsClassName = maker.getSimpleName();
		imports.add(maker.getName());
		return this;
	}
	
	protected void check() {
		if (className == null || className.trim().equals("")) {
			throw new NullPointerException("className must not be null");
		}
		if (fileName == null || fileName.trim().equals("")) {
			fileName = className + ".java";
		}
		super.check();
		if (pack == null && pack.trim().equals("")) {
			throw new NullPointerException("package must not be null");
		}
		
		if (FreeMakerUtil.hasDate(table)) {
			this.imports(Date.class);
		}
		
	}
	
	public static void main(String[] args) {
		String outPath = "E:\\project\\eclipse\\datacenter\\website_statistics\\admin\\src\\main\\base\\ldh\\base\\make\\freemaker";
//		MyMapper other = new MyMapper();
//		other.table("scenic").other("status", Status.class);
		TableInfo mi = new TableInfo(null, null, null);
		
		Table table = mi.getTable("scenic");
		
		
		new PojoWhereMaker()
		 	.pack("ldh.base.make.freemaker")
		 	.outPath(outPath)
		 	.className(FreeMakerUtil.beanName(table.getName()) + "Where")
		 	.extend(FreeMakerUtil.beanName(table.getName()))
		 	.make();
		
	}
}
