package ldh.maker.freemaker;

/**
 * Created by ldh on 2017/4/8.
 */
public class LogbackMaker extends FreeMarkerMaker<LogbackMaker> {

    protected String projectRootPackage;

    public LogbackMaker projectRootPackage(String projectRootPackage) {
        this.projectRootPackage = projectRootPackage;
        return this;
    }

    @Override
    public LogbackMaker make() {
        data();
        this.out("logback.ftl", data);
        return this;
    }

    @Override
    public void data() {
        fileName = "logback.xml";
        data.put("projectRootPackage", projectRootPackage);
    }
}
