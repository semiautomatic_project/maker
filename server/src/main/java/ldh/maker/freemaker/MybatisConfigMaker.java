package ldh.maker.freemaker;

import ldh.database.Table;
import ldh.maker.util.EnumFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * Created by ldh on 2017/4/7.
 */
public class MybatisConfigMaker extends FreeMarkerMaker<MybatisConfigMaker> {

    protected List<Table> tables;
    protected String pojoPackage;

    public MybatisConfigMaker tables(Collection<Table> tabless) {
        List<Table> ts = new ArrayList<>();
        for (Table tt : tabless) {
            if (tt.getPrimaryKey() != null && tt.getPrimaryKey().getColumn() != null) {
                ts.add(tt);
            } else {
                System.out.println("table id is error:" + tt.getName() );
            }
        }
        this.tables = ts;
        return this;
    }

    public MybatisConfigMaker pojoPackage(String pojoPackage) {
        this.pojoPackage = pojoPackage;
        return this;
    }

    @Override
    public MybatisConfigMaker make() {
        data();
        this.out("mybatis-config.ftl", data);
        return this;
    }

    @Override
    public void data() {
        fileName = "mybatis-config.xml";
        data.put("pojoPackage", pojoPackage);
        data.put("tables", tables);

        List<String> enumList = new ArrayList<>();
        for (EnumStatusMaker esm : EnumFactory.getInstance().getEnumStatusMakerMap().values()) {
            enumList.add(esm.getName());
        }
        data.put("enumList", enumList);
    }
}
