package ${projectRootPackage}.configuration;

/**
 * @author: ${Author}
 * @date: ${DATE}
 */

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile(value={"dev", "test"})
public class StaticResourceConfiguration implements WebMvcConfigurer {

    /**
    * 发现如果继承了WebMvcConfigurationSupport，则在yml中配置的相关内容会失效。
    * 需要重新指定静态资源
    * @param registry
    */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/**").addResourceLocations("classpath:/static/");
        registry.addResourceHandler("swagger-ui.html").addResourceLocations("classpath:/META-INF/resources/");
        registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
    }
}
