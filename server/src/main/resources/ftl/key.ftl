package ${package};

<#if imports?exists>
	<#list imports as import>
import ${import};
	</#list>
</#if>

<#if serializable>@SuppressWarnings("serial")</#if>
public class ${className} <#if extend?exists>extends ${extend}</#if> <#if implements?exists>implements <#list implements as implement>${implement}<#if implement_has_next>,</#if></#list></#if> ${r'{'}

	
	<#if primaryKey.columns??>
	<#list primaryKey.columns as column>
	<#if column.comment??>
	/**
	 * ${column.comment}
	 */
	</#if>
	private ${column.javaType} ${column.property};
	
	</#list>
	</#if>	
	
	<#-- ... -->
	<#if primaryKey.columns??>
	<#list primaryKey.columns as column>
	<#if column.comment??>
	/**
	 * 设置${column.comment}
	 */
	</#if>
	public void set${util.upFirst(column.property)}(${column.javaType} ${column.property}) {
		this.${column.property} = ${column.property};
	}
	
	<#if column.comment??>
	/**
	 * 获取${column.comment}
	 */
	</#if>
	public ${column.javaType} get${util.upFirst(column.property)}() {
		return ${column.property};
	}

	</#list>
	</#if>
${r'}'}

