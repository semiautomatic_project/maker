package ${package};

import de.felixroske.jfxsupport.AbstractFxmlView;
import de.felixroske.jfxsupport.FXMLView;

@FXMLView("${fxml}")
public class ${className} extends AbstractFxmlView {
}
