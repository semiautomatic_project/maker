package ldh.maker;

import ldh.maker.component.SpringJavafxContentUiFactory;
import ldh.maker.util.UiUtil;

public class SpringDeskMain extends ServerMain {

    @Override
    protected void preHandle() {
        UiUtil.setContentUiFactory(new SpringJavafxContentUiFactory());
        UiUtil.setType("springdesk");
    }

    protected String getTitle() {
        return "javafx ui frame--代码自动生成工具";
    }

    public static void main(String[] args) throws Exception {
        startDb(null);
        launch(args);
    }
}

