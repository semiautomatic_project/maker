package ${beanPackage};

 /**
 * @author: ${Author}
 * @date: ${DATE}
 * @Description: ${Description}
 */

import co.paralleluniverse.fibers.Suspendable;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.sql.ResultSet;
import io.vertx.ext.sql.SQLConnection;
import io.vertx.ext.sql.UpdateResult;
import ${projectPackage}.util.JsonObjectUtil;
import java.util.List;
import static io.vertx.ext.sync.Sync.awaitResult;

public class ${table.javaName}Dao {

    private static String SELECT = "${util.selectSql(table)}";
    private static String UPDATE_BY_ID = "${util.updateSql(table)}";
    private static String INSERT = "${util.insertSql(table)}";
    private static String COUNT = "SELECT COUNT(*) FROM ${table.name}";

    @Suspendable
    public List<JsonObject> getAll(SQLConnection connection, JsonObject jsonObject) {
        String sql = SELECT + JsonObjectUtil.limit(jsonObject);
        ResultSet query = awaitResult(h -> connection.query(sql, h));
        return query.getRows();
    }

    @Suspendable
    public List<JsonObject> list(SQLConnection connection, JsonObject jsonObject) {
        String where = JsonObjectUtil.where(jsonObject, ${util.whereParamName(table)});
        if (where.length() == 0) {
            return getAll(connection, jsonObject);
        }
        JsonArray jsonArray = JsonObjectUtil.whereParam(jsonObject, ${util.whereParamName(table)});
        String sql = SELECT + " where " + where + JsonObjectUtil.limit(jsonObject);
        ResultSet query = awaitResult(h -> connection.queryWithParams(sql, jsonArray, h));
        return query.getRows();
    }

    @Suspendable
    public Long count(SQLConnection connection, JsonObject jsonObject) {
        String where = JsonObjectUtil.where(jsonObject, ${util.whereParamName(table)});
        if (where.length() == 0) {
            JsonArray query = awaitResult(h -> connection.querySingle(COUNT, h));
            return query.size() > 0 ? query.getLong(0) : 0L;
        }
        JsonArray jsonArray = JsonObjectUtil.whereParam(jsonObject, ${util.whereParamName(table)});
        String sql = SELECT + " where " + where + JsonObjectUtil.limit(jsonObject);
        JsonArray query = awaitResult(h -> connection.querySingleWithParams(sql, jsonArray, h));
        return query.size() > 0 ? query.getLong(0) : 0L;
    }

    @Suspendable
    public void insert(SQLConnection connection, JsonObject jsonObject) {
        JsonArray jsonArray = JsonObjectUtil.toJsonArray(jsonObject, ${util.insertParamName(table)});
        UpdateResult updateResult = awaitResult(h -> connection.updateWithParams(INSERT,jsonArray,h));
        if (updateResult.getUpdated() != 1) {
            throw new RuntimeException("insert error!");
        }
//        Object id = updateResult.getKeys().getValue(0);
//        question.put("id", id);
    }

    @Suspendable
    public void update(SQLConnection connection, JsonObject jsonObject) {
        JsonArray jsonArray = JsonObjectUtil.toJsonArray(jsonObject, ${util.insertParamName(table)}, "id");
        UpdateResult updateResult = awaitResult(h -> connection.updateWithParams(UPDATE_BY_ID,jsonArray,h));
        if (updateResult.getUpdated() != 1) {
            throw new RuntimeException("insert error!");
        }
    }

    @Suspendable
    public JsonObject getById(SQLConnection connection, JsonObject jsonObject) {
        JsonArray jsonArray = JsonObjectUtil.toJsonArray(jsonObject, "id");
        String sql = SELECT + " where id = ?";
        ResultSet resultSet = awaitResult(h -> connection.queryWithParams(sql,jsonArray,h));
        List<JsonObject> jsonObjects = resultSet.getRows();
        return jsonObjects.size() > 0 ? jsonObjects.get(0) : null;
    }
}
