import 'package:flutter/material.dart';

abstract class LoadingListPage<B> extends StatefulWidget {
  LoadingListPage({Key key, this.title}) : super(key: key);

  final String title;
  _LoadingPageState _loadingPageState;

  @override
  _LoadingPageState createState() {
    _loadingPageState = new _LoadingPageState();
    return _loadingPageState;
  }

  void loadData();

  void setData(List<B> datas) {
    _loadingPageState.setDatas(datas);
  }

  Widget buildListCell(BuildContext context, B b);
}

class _LoadingPageState extends State<LoadingListPage> {

  List _datas = new List();

  @override
  Widget build(BuildContext context) {
    var length = _datas?.length ?? 0;
    return new Scaffold(
        appBar: new AppBar(
          title: new Text(widget.title),
        ),
        body: new ListView.builder(
          itemBuilder: (BuildContext context, int index) {
            if (index == length) {
              _load();
              return new Center(
                child: new Container(
                  margin: const EdgeInsets.only(top: 8.0),
                  width: 32.0,
                  height: 32.0,
                  child: const CircularProgressIndicator(),
                ),
              );
            } else if (index > length) {
              return null;
            }

            var title = _datas[index];
            Widget cell = widget.buildListCell(context, title);
            return new Container(
              decoration: new BoxDecoration(
                  border: new Border(
                      bottom: new BorderSide(color: Colors.grey.shade300)
                  )
              ),
              child: cell,
            );
          },
        )
    );
  }

  void setDatas(List datas) {
    setState(() {
      this._datas.addAll(datas);
    });
  }

  void _load() {
    widget.loadData();
  }
}

