package ldh.maker;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class MainLancher extends Application {

    public static Stage STAGE = null;

    @Override
    public void start(Stage stage) throws Exception {
        STAGE = stage;
        Parent root = FXMLLoader.load(getClass().getResource("/fxml/MainLancher.fxml"));

        Scene scene = new Scene(root);

        scene.getStylesheets().add("/styles/bootstrapfx.css");
        scene.getStylesheets().add(ServerMain.class.getResource("/styles/jfoenix-components.css").toExternalForm());
//

        stage.setTitle("自动代码生成工具");
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}